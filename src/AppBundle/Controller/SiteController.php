<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\Element;
use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectContent;
use AppBundle\Form\ChangePasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SiteController extends Controller
{
    public function indexAction()
    {
        return $this->render('site/index.html.twig');
    }

    public function catalogAction(Request $request, EntityManagerInterface $em, SessionInterface $session)
    {
        //If got an AJAX request with collection of elements for a new project
        if ($request->isXmlHttpRequest()) {
            $ids = $request->request->get('ids');
            $projectName = $request->request->get('name');
            $user = $this->getUser();

            //Getting elements and arranging them by ids
            $elements= $this->getDoctrine()
                ->getRepository(Element::class)
                ->findByIdsAndRearrangeByIds($ids);

            if ($elements != null) {
                //Creating a new project for user
                $project = new Project();
                $project->setName($projectName);
                $project->setDate(new \DateTime());
                $project->setUser($user);

                //Creating project content entities
                $i = 0;
                foreach ($ids as $id) {
                    $projectContent = new ProjectContent();
                    $projectContent->setNumber($i);
                    $projectContent->setElement($elements[$id]);
                    $projectContent->setProject($project);
                    $projectContent->setIsEmpty(true);
                    $em->persist($projectContent);
                    $i++;
                }

                //Saving changes to database
                $em->persist($project);
                $em->flush();

                $session->set('projectId', $project->getId());

                return new JsonResponse($this->generateUrl('project_index', array(
                    'projectId' => $project->getId()
                )));
            }
        }

        //Getting existing categories to represent in the template
        $categoriesRepository = $this->getDoctrine()->getRepository(Category::class);
        $categories = $categoriesRepository->findAll();

        return $this->render('site/catalog.html.twig', array(
            'categories' => $categories
        ));
    }
}
