<?php

namespace AppBundle\Service;

use AppBundle\Entity\Element;
use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectContent;
use Symfony\Component\HttpKernel\KernelInterface;

class Helper
{
    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    public function deleteDir($dirPath)
    {
        if (file_exists($dirPath)) {
            $files = glob($dirPath . '*', GLOB_MARK);
            foreach ($files as $file) {
                if (is_dir($file)) {
                    $this->deleteDir($file);
                } else {
                    unlink($file);
                }
            }
            if (file_exists($dirPath)) {
                rmdir($dirPath);
            }
        }
    }

    public function zipProject($project)
    {
        /** @var Project $project */
        $projectDir = $this->getProjectDir($project->getId());
        $indexHtml = $projectDir . "/index.html";

        if(!file_exists($projectDir)) {
            mkdir($projectDir);
        }

        file_put_contents(
            $indexHtml,
            $this->assembleProject($project)
        );

        $files = array();
        array_push($files, $this->getSamplesDir() . "/base/favicon.ico");

        array_push($files, $indexHtml);
        if (file_exists($this->getProjectDir($project->getId()) . '/images')) {
            $images = scandir($this->getProjectDir($project->getId()) . '/images');
            if ($images !== false) {
                foreach ($images as $image) {
                    $image = $this->getProjectDir($project->getId()) . '/images/' . $image;
                    array_push($files, $image);
                }
            }
        }

        $contents = $project->getContents();

        foreach ($contents as $content) {
            /** @var ProjectContent $content */
            $element = $content->getElement();
            $categoryId = $element->getCategory()->getId();
            $elementId = $element->getId();

            $cssElementFileName = $this->getSamplesDir() . '/css/' . $elementId . '.css';
            $cssCategoryFileName = $this->getSamplesDir() . '/css/_' . $categoryId . '.css';
            if (file_exists($cssElementFileName) && array_search($cssElementFileName, $files) === false) {
                array_push($files, $cssElementFileName);
            }
            if (file_exists($cssCategoryFileName) && array_search($cssCategoryFileName, $files) === false) {
                array_push($files, $cssCategoryFileName);
            }


            $jsElementFileName = $this->getSamplesDir() . '/js/' . $elementId . '.js';
            $jsCategoryFileName = $this->getSamplesDir() . '/js/_' . $categoryId . '.js';
            if (file_exists($jsElementFileName) && array_search($jsElementFileName, $files) === false) {
                array_push($files, $jsElementFileName);
            }
            if (file_exists($jsCategoryFileName) && array_search($jsCategoryFileName, $files) === false) {
                array_push($files, $jsCategoryFileName);
            }

            $dependencies = $element->getCategory()->getDependencies();
            foreach ($dependencies as $dependency) {
                $dependencyCssFileName = $this->getSamplesDir() . '/css/' . $dependency->getId() . '_.css';
                $dependencyJsFileName = $this->getSamplesDir() . '/js/' . $dependency->getId() . '_.js';
                $dependencyWoffFileName = $this->getSamplesDir() . '/woff/' . $dependency->getId() . '_.woff';
                if (file_exists($dependencyCssFileName) && array_search($dependencyCssFileName, $files) === false) {
                    array_push($files, $dependencyCssFileName);
                }
                if (file_exists($dependencyJsFileName) && array_search($dependencyJsFileName, $files) === false) {
                    array_push($files, $dependencyJsFileName);
                }
                if (file_exists($dependencyWoffFileName) && array_search($dependencyWoffFileName, $files) === false) {
                    array_push($files, $dependencyWoffFileName);
                }
            }
        }

        $zip = new \ZipArchive;
        $zipName = $this->getProjectDir($project->getId()) . "/" . $project->getId() . ".zip";
        $zip->open($zipName, \ZipArchive::CREATE);
        foreach ($files as $file) {
            $zip->addFromString(basename($file), file_get_contents($file));
        }
        $zip->close();

        return $zipName;
    }

    private function assembleProject(Project $project)
    {
        $base = file_get_contents($this->getSamplesDir() . '/base/index.html');

        $fillers = array(
            '%title%' => $project->getName(),
            '%body%' => $this->assembleBody($project),
            '%style%' => $this->assembleStyle($project),
            '%script%' =>$this->assembleScript($project)
        );

        foreach ($fillers as $key => $value ) {
            $base = str_replace($key, $value, $base);
        }

        return $base;
    }

    /**
     * @param Project $project
     * @return string
     */
    private function assembleBody(Project $project)
    {
        /** @var Project $project */
        $contents = $project->getContents();
        $body = '';
        $prevCategoryId = '';
        $isPrevElIterable = false;
        $categoryTemp = '';
        $elementTemp = '';
        $i = 0;
        foreach ($contents as $content) {
            /** @var ProjectContent $content */
            $element = $content->getElement();
            $categoryId = $element->getCategory()->getId();

            if ($i == 0) {
                $categoryTemp = file_get_contents($this->getSamplesDir() . '/html/_' . $categoryId . '.html');
                $elementTemp = $this->putContentIntoElement($element, $content);
            } else {
                if ($categoryId == $prevCategoryId &&
                    ($element->getIsIterable() == true && $element->getIsIterable() == $isPrevElIterable)) {
                    $elementTemp .= $this->putContentIntoElement($element, $content);;
                } else {
                    $body .= str_replace('%element%', $elementTemp, $categoryTemp);
                    $categoryTemp = file_get_contents($this->getSamplesDir() . '/html/_' . $categoryId . '.html');
                    $elementTemp = $this->putContentIntoElement($element, $content);;
                }
            }
            $prevCategoryId = $categoryId;
            $isPrevElIterable = $element->getIsIterable();
            $i++;
        }
        $body .= str_replace('%element%', $elementTemp, $categoryTemp);

        return $body;
    }

    private function assembleStyle(Project $project)
    {
        /** @var Project $project */
        $contents = $project->getContents();
        $blank = "<link rel=\"stylesheet\" href=\"%filename%\">";
        $style = '';

        foreach ($contents as $content) {
            /** @var ProjectContent $content */
            $element = $content->getElement();
            $categoryId = $element->getCategory()->getId();
            $elementId = $element->getId();
            $dependencies = $element->getCategory()->getDependencies();
            foreach ($dependencies as $dependency) {
                if (file_exists($this->getSamplesDir() . '/css/' . $dependency->getId() . '_.css') &&
                    strpos($style, str_replace('%filename%', $dependency->getId() . '_.css', $blank)) === false) {
                    $style .= str_replace('%filename%', $dependency->getId() . '_.css', $blank);
                }
            }
            if (file_exists($this->getSamplesDir() . '/css/_' . $categoryId . '.css') &&
                strpos($style, str_replace('%filename%', '_' . $categoryId . '.css', $blank)) === false) {
                $style .= str_replace('%filename%', '_' . $categoryId . '.css', $blank);
            }
            if (file_exists($this->getSamplesDir() . '/css/' . $elementId . '.css') &&
                strpos($style, str_replace('%filename%', $elementId . '.css', $blank)) === false) {
                $style .= str_replace('%filename%', $elementId . '.css', $blank);
            }
        }

        return $style;
    }

    private function assembleScript(Project $project)
    {
        /** @var Project $project */
        $contents = $project->getContents();
        $blank = "<script src=\"%filename%\"></script>";
        $script = '';

        foreach ($contents as $content) {
            /** @var ProjectContent $content */
            $element = $content->getElement();
            $categoryId = $element->getCategory()->getId();
            $elementId = $element->getId();
            $dependencies = $element->getCategory()->getDependencies();
            foreach ($dependencies as $dependency) {
                if (file_exists($this->getSamplesDir() . '/js/' . $dependency->getId() . '_.js') &&
                    strpos($script, str_replace('%filename%', $dependency->getId() . '_.js', $blank)) === false) {
                    $script .= str_replace('%filename%', $dependency->getId() . '_.js', $blank);
                }
            }
            if (file_exists($this->getSamplesDir() . '/js/_' . $categoryId . '.js') &&
                strpos($script, str_replace('%filename%', '_' . $categoryId . '.js', $blank)) === false) {
                $script .= str_replace('%filename%', '_' . $categoryId . '.js', $blank);
            }
            if (file_exists($this->getSamplesDir() . '/js/' . $elementId . '.js') &&
                strpos($script, str_replace('%filename%', $elementId . '.js', $blank)) === false) {
                $script .= str_replace('%filename%', $elementId . '.js', $blank);
            }
        }

        return $script;
    }

    /**
     * @return string
     */
    public function getSamplesDir()
    {
        $samplesDir = $this->kernel->getRootDir() . '/../web/samples';
        return $samplesDir;
    }

    public function getProjectDir($projectId)
    {
        $projectContentDir = $this->kernel->getRootDir()
            . '/../web/projects'
            . '/' . $projectId;

        return $projectContentDir;
    }

    /**
     * @param Element $element
     * @param ProjectContent $content
     * @return mixed|string
     */
    private function putContentIntoElement(Element $element, ProjectContent $content)
    {
        $elementHtml = file_get_contents($this->getSamplesDir() . '/html/' . $element->getId() . '.html');
        foreach ($content->getContent() as $placeholder => $value) {
            $elementHtml = str_replace('%' . $placeholder . '%', $value, $elementHtml);
        }

        return $elementHtml;
    }
}