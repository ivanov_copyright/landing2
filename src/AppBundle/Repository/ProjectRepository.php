<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProjectRepository extends EntityRepository
{
    /**
     * @param int $id
     * @return array|null
     */
    public function findByIdAndReturnAllEmptyContentsArrangedByNumber($id) {
        /** @var \AppBundle\Entity\Project $project */
        $project = $this->findOneBy(array('id' => $id));
        $contents = array();

        if ($project != null) {
            $contentsArray = $project->getContents();
            foreach ($contentsArray as $content) {
                /** @var \AppBundle\Entity\ProjectContent $content */
                if ($content->getContent() != null) {
                    $contentNumber = $content->getNumber();
                    $contents[$contentNumber] = $content;
                }
            }
            ksort($contents);
            return $contents;
        }
        return null;
    }
}