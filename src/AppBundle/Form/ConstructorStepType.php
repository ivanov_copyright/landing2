<?php

namespace AppBundle\Form;


use AppBundle\Entity\Element;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ConstructorStepType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $placeholders = $options['element']->getPlaceholders();
        foreach ($placeholders as $placeholder) {
            switch ($placeholder['type']) {
                case Element::TYPE_TEXT:
                    $builder->add($placeholder['placeholder'], TextType::class, array(
                        'attr' => array(
                            "class" => "input-text-25 margin-top-bottom-10"
                        ),
                        'label' => $placeholder['name'],
                        'constraints' => array(
                            new Length(array(
                                'min' => 3,
                                'max' => 200,
                                'minMessage' => "Поле '" . $placeholder['name'] . "' должно содержать мимнимум 3 символа",
                                'maxMessage' => "Поле '" . $placeholder['name'] . "' должно содержать максимум 50 символов"
                            )),
                            new NotBlank(array(
                                'message' => "Поле '" . $placeholder['name'] . "' не должно быть пустым'",
                            ))
                        )
                    ));
                    break;
                case Element::TYPE_TEXTAREA:
                    $builder->add($placeholder['placeholder'], TextareaType::class, array(
                        'attr' => array(
                            "class" => "input-textarea margin-top-bottom-10"
                        ),
                        'label' => $placeholder['name'],
                        'constraints' => array(
                            new Length(array(
                                'min' => 10,
                                'max' => 10000,
                                'minMessage' => "Поле '" . $placeholder['name'] . "' должно содержать мимнимум 10 символов",
                                'maxMessage' => "Поле '" . $placeholder['name'] . "' должно содержать максимум 10000 символов"
                            )),
                            new NotBlank(array(
                                'message' => "Поле '" . $placeholder['name'] . "' не должно быть пустым'"
                            ))
                        )
                    ));
                    break;
                case Element::TYPE_IMAGE:
                    $builder->add($placeholder['placeholder'], FileType::class, array(
                        'attr' => array(
                            "class" => "input-text-25 margin-top-bottom-10"
                        ),
                        'label' => $placeholder['name'],
                        'constraints' => array(
                            new File(array(
                                'binaryFormat' => false,
                                'maxSize' => '5M',
                                'maxSizeMessage' => 'Превышен допустимый лимит размера загружаемого изображения',
                                'mimeTypes' => array('image/jpeg', 'image/png'),
                                'mimeTypesMessage' => 'Допусимые форматы изображений: .jpeg, .png'
                            )),
                            new NotBlank(array(
                                'message' => "Поле '" . $placeholder['name'] . "' не должно быть пустым"
                            ))
                        ),
                    ));
                    break;
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'element' => array(),
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }

}