<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ElementRepository")
 * @ORM\Table(name="elements")
 */
class Element
{
    const TYPE_TEXT = 'text';
    const TYPE_TEXTAREA = 'textarea';
    const TYPE_IMAGE = 'image';

    private $types = array(
        self::TYPE_IMAGE,
        self::TYPE_TEXT,
        self::TYPE_TEXTAREA
    );

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="ProjectContent", mappedBy="element", cascade={"persist", "remove"})
     */
    private $projects;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Заполните поле 'Название элемента'")
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="elements")
     */
    private $category;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $placeholders;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank(message="Заполните поле 'Повтарающийся элемент?'")
     */
    private $isIterable;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->projects = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Element
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add project
     *
     * @param \AppBundle\Entity\ProjectContent $project
     *
     * @return Element
     */
    public function addProject(\AppBundle\Entity\ProjectContent $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param \AppBundle\Entity\ProjectContent $project
     */
    public function removeProject(\AppBundle\Entity\ProjectContent $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        $projects = array();

        foreach ($this->projects as $project) {
           array_push($projects, $project->getProject());
        }
        return $projects;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Element
     */
    public function setCategory(\AppBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set placeholders
     *
     * @param array $placeholders
     *
     * @return Element
     */
    public function setPlaceholders($placeholders)
    {
        $this->placeholders = json_decode($placeholders, true);

        return $this;
    }

    /**
     * Get placeholders
     *
     * @return array
     */
    public function getPlaceholders()
    {
        return $this->placeholders;
    }

    /**
     * Set isIterable
     *
     * @param boolean $isIterable
     *
     * @return Element
     */
    public function setIsIterable($isIterable)
    {
        $this->isIterable = $isIterable;

        return $this;
    }

    /**
     * Get isIterable
     *
     * @return boolean
     */
    public function getIsIterable()
    {
        return $this->isIterable;
    }
}
