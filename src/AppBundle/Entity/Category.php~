<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="Categories")
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Заполните поле 'Название категории'")
     */
    private $name;

    /**
     * @ORM\Column(type="array")
     */
    private $classesAndIds;

    /**
     * @ORM\OneToMany(targetEntity="Element", mappedBy="category", cascade={"persist", "remove"})
     */
    private $elements;

    /**
     * @ORM\ManyToMany(targetEntity="Dependency", inversedBy="categories")
     */
    private $dependencies;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->elements = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dependencies = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dependencies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add element
     *
     * @param \AppBundle\Entity\Element $element
     *
     * @return Category
     */
    public function addElement(\AppBundle\Entity\Element $element)
    {
        $this->elements[] = $element;

        return $this;
    }

    /**
     * Remove element
     *
     * @param \AppBundle\Entity\Element $element
     */
    public function removeElement(\AppBundle\Entity\Element $element)
    {
        $this->elements->removeElement($element);
    }

    /**
     * Get elements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * Add dependency
     *
     * @param \AppBundle\Entity\Dependency $dependency
     *
     * @return Category
     */
    public function addDependency(\AppBundle\Entity\Dependency $dependency)
    {
        $this->dependencies[] = $dependency;

        return $this;
    }

    /**
     * Remove dependency
     *
     * @param \AppBundle\Entity\Dependency $dependency
     */
    public function removeDependency(\AppBundle\Entity\Dependency $dependency)
    {
        $this->dependencies->removeElement($dependency);
    }

    /**
     * Get dependencies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDependencies()
    {
        return $this->dependencies;
    }

    /**
     * Set classesAndIds
     *
     * @param array $classesAndIds
     *
     * @return Category
     */
    public function setClassesAndIds($classesAndIds)
    {
        $this->ClassesAndIds = $classesAndIds;

        return $this;
    }

    /**
     * Get classesAndIds
     *
     * @return array
     */
    public function getClassesAndIds()
    {
        return $this->ClassesAndIds;
    }
}
