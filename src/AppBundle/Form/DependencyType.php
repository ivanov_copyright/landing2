<?php

namespace AppBundle\Form;

use AppBundle\Entity\Dependency;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DependencyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'required' => true
            ))
            ->add('jsFile', FileType::class,array(
                'required' => false,
                'mapped' => false
            ))
            ->add('cssFile', FileType::class,array(
                'required' => false,
                'mapped' => false
            ))
            ->add('woffFile', FileType::class,array(
                'required' => false,
                'mapped' => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Dependency::class,
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }
}