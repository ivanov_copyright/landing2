<?php

namespace AppBundle\Form;

use AppBundle\Entity\Category;
use AppBundle\Entity\Element;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ElementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $options['em'];
        $categories = $em->getRepository(Category::class)->findAll();
        $categories_choices = array();

        /** @var \AppBundle\Entity\Category $category*/
        foreach ($categories as $category ) {
            $categories_choices[$category->getName()] = $category;
        }

        $builder
            ->add('category', ChoiceType::class, array(
                'required' => true,
                'placeholder' => 'Выберете категорию',
                'choices' => $categories_choices,
                'error_bubbling' => true
            ))
            ->add('name', TextType::class, array(
                'required' => true,
                'error_bubbling' => true
            ))
            ->add('isIterable', ChoiceType::class, array(
                'required' => true,
                'placeholder' => 'Повторяющийся элемент?',
                'choices' => array(
                    'Да' => 1,
                    'Нет' => 0
                ),
                'error_bubbling' => true
            ))
            ->add('placeholders', TextType::class, array(
                'required' => true,
                'error_bubbling' => true
            ))
            ->add('image', FileType::class,array(
                'required' => true,
                'mapped' => false
            ))
            ->add('htmlFile', FileType::class,array(
                'required' => false,
                'mapped' => false
            ))
            ->add('jsFile', FileType::class,array(
                'required' => false,
                'mapped' => false
            ))
            ->add('cssFile', FileType::class,array(
                'required' => false,
                'mapped' => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Element::class,
            'em' => null,
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }
}