<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ConstructorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $elementsInsides = $options['elementsInsides'];

        if ($elementsInsides['heading'] == true) {
            $builder->add('heading', TextType::class, array(
                'required' => true,
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 3, 'max' => 50)),
                ),
            ));
        }
        if ($elementsInsides['content'] == true) {
            $builder->add('heading', TextType::class, array(
                'required' => true,
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 10, 'max' => 10000)),
                ),
            ));
        }
        if ($elementsInsides['image'] == true) {
            $builder->add('heading', FileType::class, array(
                'required' => true,
                'constraints' => array(
                    new Image(array('maxSize' => "10M"))
                ),
            ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'elementsInsides' => null,
        ));
    }
}