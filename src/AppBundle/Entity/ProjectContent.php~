<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectContentRepository")
 * @ORM\Table(name="project_contents")
 */
class ProjectContent
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Element", inversedBy="projects")
     */
    private $element;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="contents")
     */
    private $project;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\NotBlank()
     */
    private $number;

    /**
     * @ORM\Column(type="array")
     */
    private $content;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEmpty;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return ProjectContent
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set content
     *
     * @param array $content
     *
     * @return ProjectContent
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return array
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set element
     *
     * @param \AppBundle\Entity\Element $element
     *
     * @return ProjectContent
     */
    public function setElement(\AppBundle\Entity\Element $element = null)
    {
        $this->element = $element;

        return $this;
    }

    /**
     * Get element
     *
     * @return \AppBundle\Entity\Element
     */
    public function getElement()
    {
        return $this->element;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project $project
     *
     * @return ProjectContent
     */
    public function setProject(\AppBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }
}
