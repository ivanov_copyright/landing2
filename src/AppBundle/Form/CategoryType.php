<?php

namespace AppBundle\Form;

use AppBundle\Entity\Category;
use AppBundle\Entity\Dependency;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $options['em'];
        $dependencies = $em->getRepository(Dependency::class)->findAll();
        $dependencies_choices = array();

        /** @var \AppBundle\Entity\Category $category*/
        foreach ($dependencies as $dependency ) {
            $dependencies_choices[$dependency->getName()] = $dependency;
        }

        $builder
            ->add('name', TextType::class, array(
                'required' => true
            ))
            ->add('dependencies', ChoiceType::class, array(
                'mapped' => false,
                'required' => true,
                'multiple' => true,
                'choices' => $dependencies_choices
            ))
            ->add('htmlFile', FileType::class,array(
                'required' => false,
                'mapped' => false
            ))
            ->add('jsFile', FileType::class,array(
                'required' => false,
                'mapped' => false
            ))
            ->add('cssFile', FileType::class,array(
                'required' => false,
                'mapped' => false
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Category::class,
            'em' => null,
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }
}