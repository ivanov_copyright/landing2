<?php
namespace AppBundle\Controller;


use AppBundle\Entity\Category;
use AppBundle\Entity\Dependency;
use AppBundle\Entity\Element;
use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use AppBundle\Form\CategoryType;
use AppBundle\Form\DependencyType;
use AppBundle\Form\ElementType;
use AppBundle\Service\Helper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    public function indexAction(EntityManagerInterface $em)
    {
        $userRepo = $em->getRepository(User::class);
        $users = $userRepo->findAll();

        $projectRepo = $em->getRepository(Project::class);
        $projects = $projectRepo->findAll();

        $elementRepo = $em->getRepository(Element::class);
        $elements = $elementRepo->findAll();

        $categoryRepo = $em->getRepository(Category::class);
        $categories = $categoryRepo->findAll();

        $dependencyRepo = $em->getRepository(Dependency::class);
        $dependencies = $dependencyRepo->findAll();

        return $this->render('admin/index.html.twig', array(
            'users' => $users,
            'projects' => $projects,
            'elements' => $elements,
            'categories' => $categories,
            'dependencies' => $dependencies
        ));
    }

    public function addCategoryAction(Request $request, EntityManagerInterface $em, Helper $helper)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category, array("em" => $em));
        $message = '';

        //Handle form submit (on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if($form->isValid()) {
                if ($form['dependencies']->getData()) {
                    $dependencies = $form['dependencies']->getData();
                    foreach ($dependencies as $dependency) {
                        $category->addDependency($dependency);
                        $dependency->addCategory($category);
                    }
                }
                $em->persist($category);
                $em->flush();

                $files = array();
                $cssClassesAndIds = array(
                    'raw' => array(),
                    'cut' => array()
                );

                //getting files from form and putting files with ID names to samples directories
                if ($form['htmlFile']->getData()) {
                    $form['htmlFile']->getData()->move($helper->getSamplesDir() . '/html', '_' . $category->getId() . '.html');

//                    array_push($files, $helper->getSamplesDir() . '/html/_' . $category->getId() . '.html');
                    $files['html'] = $helper->getSamplesDir() . '/html/_' . $category->getId() . '.html';
                }
                if ($form['cssFile']->getData()) {
                    $form['cssFile']->getData()->move($helper->getSamplesDir() . '/css', '_' . $category->getId() . '.css');

                    $cssFileName = $helper->getSamplesDir() . '/css/_' . $category->getId() . '.css';
                    $cssClassesAndIds = $this->parseCssForClassesAndIds($cssFileName);
//                    array_push($files, $cssFileName);
                    $files['css'] = $cssFileName;
                }
                if ($form['jsFile']->getData()) {
                    $form['jsFile']->getData()->move($helper->getSamplesDir() . '/js', '_' . $category->getId() . '.js');

//                    array_push($files, $helper->getSamplesDir() . '/js/_' . $category->getId() . '.js');
                    $files['js'] = $helper->getSamplesDir() . '/js/_' . $category->getId() . '.js';
                }

                if ( !empty($cssClassesAndIds)) {
                    foreach ($files as $key => $file) {
                        $fileString = file_get_contents($file);
                        if ($key === 'html') {
                            foreach ($cssClassesAndIds['cut'] as $item) {
                                $fileString = str_replace($item, $item.'_'.$category->getId(),$fileString);
                            }
                        } else {
                            foreach ($cssClassesAndIds['raw'] as $item) {
                                $fileString = str_replace($item, $item.'_'.$category->getId(),$fileString);
                            }
                        }
                        file_put_contents($file, $fileString);
                    }
                }


                $category = $em->getRepository(Category::class)->find($category->getId());
                $category->setClassesAndIds($cssClassesAndIds);
                $em->persist($category);
                $em->flush();

                $message = 'Категория успешно добавлена';
            } else {
                //Creating an array with errors
                $message = $form->getErrors(true);
            }
        }

        //Checking for existing categories
        $repository = $this->getDoctrine()->getRepository(Category::class);
        $categories = $repository->findBy(array(), array('id' => 'DESC'));

        return $this->render('admin/addCategory.html.twig', array(
            'form' => $form->createView(),
            'categories' => $categories,
            'message' => $message
        ));
    }

    public function addElementAction(Request $request, EntityManagerInterface $em, Helper $helper)
    {
        $message = '';

        $element = new Element();
        $form = $this->createForm(ElementType::class, $element, array('em' => $em));

        //Handle form submit (on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                //Saving elements' data to database
                $em->persist($element);
                $em->flush();

                $files = array();
                $cssClassesAndIds = array(
                    'raw' => array(),
                    'cut' => array()
                );

                //getting files from form and putting files with ID names to samples directories
                if ($form['image']->getData()) {
                    $image = $form['image']->getData();
                    $image->move($helper->getSamplesDir() . '/img', $element->getId() . '.' . $image->guessExtension());
                }
                if ($form['htmlFile']->getData()) {
                    $form['htmlFile']->getData()->move($helper->getSamplesDir() . '/html', $element->getId() . '.html');

//                    array_push($files, $helper->getSamplesDir().'/html/'.$element->getId().'.html');
                    $files['html'] = $helper->getSamplesDir().'/html/'.$element->getId().'.html';
                }
                if ($form['cssFile']->getData()) {
                    $form['cssFile']->getData()->move($helper->getSamplesDir() . '/css', $element->getId() . '.css');

                    $cssFileName = $helper->getSamplesDir().'/css/'.$element->getId().'.css';
                    $cssClassesAndIds = $this->parseCssForClassesAndIds($cssFileName);

//                    array_push($files, $helper->getSamplesDir().'/css/'.$element->getId().'.css');
                    $files['css'] = $helper->getSamplesDir().'/css/'.$element->getId().'.css';
                }
                if ($form['jsFile']->getData()) {
                    $form['jsFile']->getData()->move($helper->getSamplesDir() . '/js', $element->getId() . '.js');

//                    array_push($files, $helper->getSamplesDir().'/js/'.$element->getId().'.js');
                    $files['js'] = $helper->getSamplesDir().'/js/'.$element->getId().'.js';
                }

                $categoryCssClassesAndIds = $element->getCategory()->getClassesAndIds();
                $cssClassesAndIds['raw'] = array_merge($cssClassesAndIds['raw'], $categoryCssClassesAndIds['raw']);
                $cssClassesAndIds['raw'] = array_unique($cssClassesAndIds['raw']);
                $cssClassesAndIds['cut'] = array_merge($cssClassesAndIds['cut'], $categoryCssClassesAndIds['cut']);
                $cssClassesAndIds['cut'] = array_unique($cssClassesAndIds['cut']);
                if ( !empty($cssClassesAndIds)) {
                    foreach ($files as $key => $file) {
                        $fileString = file_get_contents($file);
                        if ($key === 'html') {
                            foreach ($cssClassesAndIds['cut'] as $item) {
                                $fileString = str_replace($item, $item.'_'.$element->getCategory()->getId(),$fileString);
                            }
                        } else {
                            foreach ($cssClassesAndIds['raw'] as $item) {
                                $fileString = str_replace($item, $item.'_'.$element->getCategory()->getId(),$fileString);
                            }
                        }
                        file_put_contents($file, $fileString);
                    }
                }


                $message = 'Элемент успешно добавлен';
            } else {
                //Creating an array with errors
                $message = $form->getErrors(true);
            }
        }

        //Checking existing elements
        $repository = $this->getDoctrine()->getRepository(Element::class);
        $elements = $repository->findBy(array(), array('id' => 'DESC'));

        return $this->render('admin/addElement.html.twig', array(
            'form' => $form->createView(),
            'message' => $message,
            'elements' => $elements,
            'types' => $element->getTypes()
        ));
    }

    public function addDependencyAction(Request $request, EntityManagerInterface $em, Helper $helper)
    {
        $dependency = new Dependency();
        $form = $this->createForm(DependencyType::class, $dependency);
        $message = '';

        //Handle form submit (on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em->persist($dependency);
                $em->flush();

                //Move dependency files to samples directory
                if ($form['cssFile']->getData()) {
                    $form['cssFile']->getData()->move(
                        $helper->getSamplesDir() . '/css',
                        $dependency->getId() . '_.css'
                    );
                };
                if ($form['jsFile']->getData()) {
                    $form['jsFile']->getData()->move(
                        $helper->getSamplesDir() . '/js',
                        $dependency->getId() . '_.js'
                    );
                };
                if ($form['woffFile']->getData()) {
                    $font = $form['woffFile']->getData();
                    $font->move(
                        $helper->getSamplesDir() . '/woff',
                        $dependency->getId() . '_.woff'
                    );
                    $fontCss = file_get_contents($helper->getSamplesDir() . '/css/' . $dependency->getId() . '_.css');
                    if ($fontCss !== false) {
                        $fontCss = str_replace('%font%', $dependency->getId() . '_.woff', $fontCss);
                        file_put_contents($helper->getSamplesDir() . '/css/' . $dependency->getId() . '_.css', $fontCss);
                    }
                }

                $message = 'Зависимость успешно добавлена';
            } else {
                //Creating an array with errors
                $message = $form->getErrors(true);
            }
        }

        //Checking what dependency files already exist
        $repository = $this->getDoctrine()->getRepository(Dependency::class);
        $dependencies = $repository->findBy(array(), array('id' => 'DESC'));

        return $this->render('admin/addDependency.twig', array(
            'dependencies' => $dependencies,
            'form' => $form->createView(),
            'message' => $message
        ));
    }

    public function deleteDependencyAction(Helper $helper, EntityManagerInterface $em, $id)
    {
        $this->deleteDependency($helper, $em, $id);

        return $this->redirectToRoute('admin_index');
    }

    public function deleteCategoryAction(Helper $helper, EntityManagerInterface $em, $id)
    {
        $this->deleteCategory($helper, $em, $id);

        return $this->redirectToRoute('admin_index');
    }

    public function deleteElementAction(Helper $helper, EntityManagerInterface $em, $id)
    {
        $this->deleteElement($helper, $em, $id);

        return $this->redirectToRoute('admin_index');
    }

    private function deleteDependency(Helper $helper, EntityManagerInterface $em, $id)
    {
        $dependency = $this->get('doctrine')
            ->getManager()
            ->getRepository(Dependency::class)
            ->find($id);

        /** @var Dependency $dependency */
        $categories = $dependency->getCategories();
        foreach ($categories as $category) {
            $this->deleteCategory($helper, $em, $category->getId());
        }

        $this->deleteDependencyFiles($helper, $id);

        $em->remove($dependency);
        $em->flush();
    }

    private function deleteCategory(Helper $helper, EntityManagerInterface $em, $id)
    {
        $category = $this->get('doctrine')
            ->getManager()
            ->getRepository(Category::class)
            ->find($id);

        /** @var Category $category */
        $elements = $category->getElements();

        $this->deleteCategoryFiles($helper, $id);

        foreach ($elements as $element)
        {
            $this->deleteElementFiles($helper, $element->getid());
            $projects = $element->getProjects();
            foreach ($projects as $project) {
                $helper->deleteDir($helper->getProjectDir($project->getId()));
                $em->remove($project);
                $em->flush();
            }
        }

        $em->remove($category);
        $em->flush();
    }

    private function deleteElement(Helper $helper, EntityManagerInterface $em, $id)
    {
        /** @var Element $element */
        $element = $this->get('doctrine')
            ->getManager()
            ->getRepository(Element::class)
            ->find($id);

        $projects = $element->getProjects();
        foreach ($projects as $project) {
            $helper->deleteDir($helper->getProjectDir($project->getId()));
            $em->remove($project);
            $em->flush();
        }

        $this->deleteElementFiles($helper, $id);

        $em->remove($element);
        $em->flush();
    }

    private function deleteDependencyFiles(Helper $helper, $id)
    {
        $files = array(
            $helper->getSamplesDir() . '/html/' . $id . '_.html',
            $helper->getSamplesDir() . '/woff/' . $id . '_.woff',
            $helper->getSamplesDir() . '/css/' . $id . '_.css',
            $helper->getSamplesDir() . '/js/' . $id . '_.js',
        );

        foreach ($files as $file) {
            if (file_exists($file)) {
                unlink($file);
            }
        }
    }

    private function deleteCategoryFiles(Helper $helper, $id)
    {
        $files = array(
            $helper->getSamplesDir() . '/html/_' . $id . '.html',
            $helper->getSamplesDir() . '/css/_' . $id . '.css',
            $helper->getSamplesDir() . '/js/_' . $id . '.js',
        );

        foreach ($files as $file) {
            if (file_exists($file)) {
                unlink($file);
            }
        }
    }

    private function deleteElementFiles(Helper $helper, $id)
    {
        $files = array(
            $helper->getSamplesDir() . '/img/' . $id . '.jpeg',
            $helper->getSamplesDir() . '/img/' . $id . '.jpg',
            $helper->getSamplesDir() . '/img/' . $id . '.png',
            $helper->getSamplesDir() . '/html/' . $id . '.html',
            $helper->getSamplesDir() . '/css/' . $id . '.css',
            $helper->getSamplesDir() . '/js/' . $id . '.js',
        );

        foreach ($files as $file) {
            if (file_exists($file)) {
                unlink($file);
            }
        }
    }

    private function parseCssForClassesAndIds($filename)
    {
        $classesAndIdsRaw = array();
        $classesAndIdsCut = array();

        $css = file_get_contents($filename);

        $pattern = '/(?:\.|#)[a-zA-Z0-9_-]+/';

        preg_match_all($pattern, $css, $classesAndIdsRaw);
        $classesAndIdsRaw = $classesAndIdsRaw[0];

        foreach ($classesAndIdsRaw as $item) {
            $item = strtr($item,array(
                '.' => '',
                '#' => ''
            ));
            array_push($classesAndIdsCut, $item);
        }

        $classesAndIds = array(
            'raw' => $classesAndIdsRaw,
            'cut' => $classesAndIdsCut
        );

        return $classesAndIds;
    }
}