<?php

namespace AppBundle\Repository;


use AppBundle\Entity\ProjectContent;
use Doctrine\ORM\EntityRepository;

class ProjectContentRepository extends EntityRepository
{
    /**
     * @param $project
     * @param int $step
     * @return \AppBundle\Entity\ProjectContent
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getProjectStep($project, $step = 0)
    {
        return $this->_em->createQueryBuilder()
            ->select('project_content')
            ->from('AppBundle:', 'project_content')
            ->where('project_content.project = :project')
            ->andWhere('project_content.number = :number')
            ->setParameters(array(
                'project' => $project,
                'number' => $step,
            ))
            ->getQuery()
            ->getSingleResult();
    }
}