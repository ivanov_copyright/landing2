<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectContent;
use AppBundle\Form\ConstructorStepType;
use AppBundle\Service\Helper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProjectController extends Controller
{
    public function indexAction($projectId)
    {
        $project = $this->getProject($projectId);
        $downloadable = false;

        $filledContents = 0;
        $contents = $project->getContents();
        foreach ($contents as $content) {
            /** @var ProjectContent $content */
            if ($content->getIsEmpty() !== true) {
                $filledContents++;
            }
        }
        if ($filledContents == count($contents)) {
            $downloadable = true;
        }

        return $this->render('project/index.html.twig', array(
            'project' => $project,
            'downloadable' => $downloadable
        ));
    }

    public function constructorStepAction(Helper $helper, Request $request, EntityManagerInterface $em, $projectId, $step)
    {
        $project = $this->getProject($projectId);
        $errors = array();
        
        try {
            $project_content = $em->getRepository(ProjectContent::class)
                ->getProjectStep($project, $step);
        } catch (\Exception $e) {
            throw $this->createNotFoundException();
        }
        
        $form = $this->createForm(
            ConstructorStepType::class, 
            null,
            array('element' => $project_content->getElement())
        );
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $project_content->setContent($this->processData($helper, $form->getData(), $projectId, $step));
                $project_content->setIsEmpty(false);
                $em->persist($project_content);
                $em->flush();

                return $this->redirect($this->generateUrl('project_index', array('projectId' => $projectId)));
            } else {
                //Creating an array with errors
                $errors = explode('ERROR: ', $form->getErrors(true));
            }
        }

        return $this->render('project/constructorStep.html.twig', array(
            'form' => $form->createView(),
            'errors' => $errors,
            'project_content' => $project_content
        ));
    }

    public function downloadAction(Helper $helper, $projectId)
    {
        $project = $this->getProject($projectId);

        $zipName = $helper->zipProject($project);

        $response = new Response(file_get_contents($zipName));
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . basename($zipName) . '"');
        $response->headers->set('Content-length', filesize($zipName));

        return $response;
    }

    public function deleteAction(Helper $helper, EntityManagerInterface $em, $projectId)
    {
        $project = $this->getProject($projectId);
        try {
            $helper->deleteDir($helper->getProjectDir($projectId));
        } catch (\Exception $e) {
        }

        $em->remove($project);
        $em->flush();

        return $this->redirectToRoute('profile');
    }
    
    private function getProject($id)
    {
        $project = $this->getDoctrine()
            ->getManager()
            ->getRepository(Project::class)
            ->find($id);

        if (!($project instanceof Project) || $project->getUser()->getId() != $this->getUser()->getId()) {
            throw $this->createNotFoundException();
        }
        
        return $project;
    }

    private function processData(Helper $helper, array $data, $projectId, $step)
    {
        $toDB = array();

        foreach ($data as $key => $value) {
            if (is_a($value, '\Symfony\Component\HttpFoundation\File\UploadedFile')) {
                /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $value */
                $fileName = $step . '_' . $key . '.' . $value->guessExtension();
                $value->move($helper->getProjectDir($projectId) . '/images', $step . '_' . $key . '.' . $value->guessExtension());
                $toDB[$key] = $fileName;
            } else {
                $toDB[$key] = $value;
            }
        }

        return $toDB;
    }
}