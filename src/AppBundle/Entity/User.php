<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_users")
 * @UniqueEntity(fields="email", message="Пользователь с такой эл.почтой уже зарегистрирован", groups={"registration"})
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="user", cascade={"persist", "remove"})
     */
    private $projects;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank(
     *     groups={"registration"},
     *     message="Поле 'Эл.почта' не должно быть пустым"
     *     )
     * @Assert\Email(
     *     groups={"registration"},
     *     message="Поле 'Эл.почта' должно содержать ральный адрес электронной почты"
     *     )
     */
    private $email;

    /**
     * @ORM\Column(type="array")
     */
    private $roles = array();

    /**
     * @Assert\NotBlank(
     *     groups={"registration", "change_password"},
     *     message="Пароль должен содержать 3-100 символов, состоять из латинских букв и\или цифр"
     *     )
     * @Assert\Regex(
     *     pattern="/^([a-zA-Z0-9-_]{3,100})$/",
     *     message="Пароль должен содержать 3-100 символов, состоять из латинских букв и\или цифр",
     *     groups={"registration"}
     *     )
     */
    private $plainPassword;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    public function getProjects()
    {
        return $this->projects;
    }

    public function setProjects($projects)
    {
        $this->projects = $projects;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getSalt()
    {
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        if (in_array('ROLE_USER', $this->roles)) {
            return $this->roles;
        } else {
            return array_merge(array('ROLE_USER'), $this->roles);
        }
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password
            ) = unserialize($serialized);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}
