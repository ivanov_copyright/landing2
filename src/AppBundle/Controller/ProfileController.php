<?php

namespace AppBundle\Controller;


use AppBundle\Form\ChangePasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfileController extends Controller
{
    public function indexAction()
    {
        /** @var \AppBundle\Entity\User $user */
        $user = $this->getUser();
        $projects = $user->getProjects();

        return $this->render('profile/index.html.twig', array(
            'user' => $user,
            'projects' => $projects
        ));
    }
}