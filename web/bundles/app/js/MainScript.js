function confirm() {
    $('.remove').click(function () {
        var action = $(this).data('action');
        $('#confirmation-modal').modal('toggle');
        $('#agree').click(function () {
            window.location.href = action;
        });
    });
}

//Makes element blink
function blink(el, speed, repeat, delay) {
    if (typeof(repeat)==='undefined' || repeat < 1) repeat = 1;
    if (typeof(delay)==='undefined' || delay < 0) delay = 0;
    setTimeout(function () {
        for (var i = 0; i < repeat; i++ ) {
            $(el).fadeOut(speed / 2).fadeIn(speed / 2);
        }
    }, delay);

}

//Makes element appear from left or right
function appear(el, side, delay, speed, callback) {
    // Setting default values
    if (typeof(callback)==='undefined') callback = function () {};
    if (typeof(side)==='undefined') side = 'left';
    if (typeof(delay)==='undefined' || delay < 0) delay = 0;
    if (typeof(speed)==='undefined' || speed < 0) speed = 1200;

    var defaultMargin = $(el).css('margin-left');
    var sideIndex;
    if (side == 'left') {
        sideIndex = -1;
    } else if (side == 'right') {
        sideIndex = 1;
    } else {
        sideIndex = -1;
    }
    var windowWidth = $(window).width();

    //Perform animation on wide screen
    if (windowWidth > 1100) {
        $(el)
            .css('opacity', 0)
            .delay(delay)
            .css('margin-left', (windowWidth / 2) * sideIndex)
            .animate({
                marginLeft: defaultMargin,
                opacity: 1
            }, speed, 'swing', function () {
                callback();
            });
    //Perform animation on narrow screen
    } else {
        $(el)
            .css('opacity', 0)
            .delay(delay)
            .animate({
                opacity: 1
            }, speed, 'swing', function () {
                callback();
            });
    }
}

//Makes arrow appear from top and stop right before the heading
function arrowDown(speed, delay) {

    //Setting default values
    if (typeof(speed)==='undefined') speed = 1500;
    if (typeof(delay)==='undefined') delay = 0;
    var arrow = $('#arrow');
    var headingPosition = $('.heading-1').position();

    //Animating the arrow
    arrow.css({
        'left': ($(window).width() - arrow.width()) / 2,
        'top' : -arrow.height(),
        'visibility' : 'visible',
        'opacity' : 0
    });
    arrow
        .delay(delay)
        .animate({
            top: headingPosition.top - arrow.height() - 15,
            opacity: 1
        }, speed, 'swing');
}

//Preform scrolling to the top of scrollable element when mouse leave the element
function scrollBack(el) {
    $(el).mouseleave(function () {
        $(this).animate({
            scrollTop: 0
        }, 1000);
    })
};

// Resize image in text block to current window settings
function textBlockResize () {
    var windowWidth = $(window).width();
    $('.text-block-image-container').each(function() {

        // Defining DOM-element variables inside each text block element
        var imageContainer = $(this);
        var textContainer = imageContainer.siblings();
        var textHeading = textContainer.children('.text-block-heading');
        var textMainText = textContainer.children('.text-block-main-text');

        // Defining total height of containers. Height of both elements in a row is equal to its width (1 X 1)
        var totalHeight = imageContainer.width();

        // Calculate main text height in a text container so it could fit in it
        var mainTextHeight = totalHeight - textHeading.outerHeight();

        //Apply all measurements to elements
        imageContainer.css({
            'height' : totalHeight + 'px'
        });
        textContainer.css({
            'height' : totalHeight + 'px'
        });
        textMainText.css({
            'height' : mainTextHeight + 'px'
        });

        //Add extra padding under text block in case of using on mobile devices
        if (windowWidth < 768) {
            console.log(windowWidth);
            textMainText.css({
                'padding-bottom' : 20 + 'px'
            });
        }
    });
}

//Dim everything on page on click, and if el is a link, then navigate to url.
function dim (el, speed) {
    if (typeof(speed)==='undefined') speed = 300;
    var element = $(el);
    element.click(function (e) {
        e.preventDefault();
        var a = $(this);
        $('body').animate({
            opacity: 0
        }, speed, 'swing', function () {
            if (el === 'a') {
                window.location.href = a.attr('href');
            }
        });
    });
}

//Slide up the slideEl after clicking on clickEl
function slideUp (clickEl, slideEl, speed) {
    if (typeof(slideEl)==='undefined') slideEl = clickEl;
    if (typeof(speed)==='undefined') speed = 500;
    $(clickEl).click(function () {
        $(slideEl).slideUp(speed, function(){ $(slideEl).remove(); });
    });
}

//Switch "src" attribute of hovered element on mouse over
function switchSrcOnHover(el, src) {
    var element = $(el);
    var defaultSrc = element.attr('src');
    element.hover(
        function () {
            $(this).attr("src", src);
        },
        function () {
            $(this).attr("src", defaultSrc);
        }
    );
}

//Shifts the element to right on click
function shiftAnimationOnClick(el, offset, speed) {
    if (typeof(speed)==='undefined') speed = 50;
    if (typeof(offset)==='undefined') offset = 30;
    $(el).click(function () {
        $(this).animate({paddingLeft: offset}, speed, 'swing', function () {
            $(this).animate({paddingLeft: 0}, (speed * 2));
        });
    });
}