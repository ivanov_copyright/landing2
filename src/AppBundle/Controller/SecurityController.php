<?php

namespace AppBundle\Controller;


use AppBundle\Entity\User;
use AppBundle\Form\ChangePasswordType;
use AppBundle\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em)
    {
        //Redirecting to main page if already registered
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('site_homepage');
        }

        $message = '';

        //Build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        //Handle the submit (on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                //Encode the password
                $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);

                //Save the user
                $em->persist($user);
                $em->flush();

                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));

                return $this->redirectToRoute('site_catalog');
            } else {
                //Creating an array with errors
                $message = explode('ERROR: ', $form->getErrors(true));
            }
        }

        return $this->render('security/register.html.twig', array(
            'form' => $form->createView(),
            'message' => $message
        ));
    }

    public function loginAction(Request $request, AuthenticationUtils $authUtils)
    {
        //Redirecting to main page if already registered
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('site_homepage');
        }

        // Get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();
        if ($error && $error->getMessage() === 'Bad credentials.') {
            $error = 'Введен неправильный пароль или эл.почта';
        }

        //Last username(email) entered by the user
        $lastUserName = $authUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUserName,
            'error' => $error
        ));
    }

    public function changePasswordAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em)
    {
        /** @var \AppBundle\Entity\User $user */
        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordType::class, $user);
        $message = '';
        $form->handleRequest($request);

        //Changing password
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $oldPassword = $form['oldPassword']->getData();
                $newPassword = $form['plainPassword']->getData();
                if (preg_match('/^([a-zA-Z0-9-_]{3,100})$/', $newPassword) === 1) {
                    if ($passwordEncoder->isPasswordValid($user, $oldPassword)) {
                        $password = $passwordEncoder->encodePassword($user, $newPassword);
                        $user->setPassword($password);

                        $em->persist($user);
                        $em->flush();

                        $message .= 'Пароль успешно изменен';
                    } else {
                        $message .= 'Неверный старый пароль';
                    };
                } else {
                    $message .= 'Пароль должен содержать 3-100 символов, состоять из латинских букв и\или цифр';
                }
            } else {
                $message .= $form->getErrors(true);
            }
        }

        return $this->render('security/changePassword.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
            'message' => $message
        ));
    }

}